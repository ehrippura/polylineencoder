# PolylineEncoder

Implements Google's polyline compression algorithm.

For more details refer to the algorithm definition at [here] (https://developers.google.com/maps/documentation/utilities/polylinealgorithm)


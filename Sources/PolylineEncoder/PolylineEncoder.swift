/**
 Copyright Wayne Lin <ehrippura@gmail.com>

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

import Foundation

private extension StringProtocol {
    subscript(offset: Int) -> Character {
        self[index(startIndex, offsetBy: offset)]
    }
}

public struct PolylineEncoder {

    private var points: [Coordinate] = []

    public static func encode(points: [Coordinate]) -> String {
        var enc = PolylineEncoder()
        for p in points {
            enc.add(p)
        }
        return enc.encode()
    }

    public static func decode(encoded: String) -> [Coordinate] {

        var idx = 0
        var result: [Coordinate] = []

        let decodeBody: () -> Double = {
            var result: Int32 = 0
            var shift: Int32 = 0
            var charScalar: UInt8 = 0

            repeat {
                charScalar = encoded[idx].asciiValue!
                idx += 1
                charScalar -= Constant.asciiOffset
                result |= Int32(charScalar & Constant.fiveBitMask) << shift
                shift += Constant.chunkSize
            } while charScalar >= Constant.orMask

            if result & 1 > 0 {
                result = ~result
            }

            result >>= 1

            return Double(result) / Constant.precision
        }

        while idx < encoded.count {
            var lat = decodeBody()
            var lon = decodeBody()
            if let last = result.last {
                lat += last.latitude
                lon += last.longitude
            }
            result.append(.init(latitude: lat, longitude: lon))
        }

        return result
    }

    public mutating func add(_ point: Coordinate) {
        if point.isValid() {
            points.append(point)
        }
    }

    public mutating func clean() {
        points.removeAll()
    }

    private func encode(val: Int32) -> String {

        var value = val << 1        // step 4

        if (val < 0) {
            value = ~value          // step 5
        }

        var result = ""

        repeat {
            var next: UInt8 = UInt8(value & Int32(Constant.fiveBitMask))
            value >>= Constant.chunkSize
            if value > 0 {
                next |= Constant.orMask
            }
            next += Constant.asciiOffset
            result.append(Character(Unicode.Scalar(next)))
        } while value > 0

        return result
    }

    /// encode polyline string
    /// https://developers.google.com/maps/documentation/utilities/polylinealgorithm
    public func encode() -> String {

        var xPrev: Int32 = 0
        var yPrev: Int32 = 0

        return points.reduce(into: "") { (result, point) in

            let latitude = Int32(round(point.latitude * Constant.precision))
            let longitude = Int32(round(point.longitude * Constant.precision))

            result += encode(val: latitude - yPrev) + encode(val: longitude - xPrev)

            xPrev = longitude
            yPrev = latitude
        }
    }
}

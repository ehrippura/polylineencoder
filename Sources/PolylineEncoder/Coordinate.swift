//
//  File.swift
//  
//
//  Created by wayne.lin on 2020/11/3.
//

#if os(iOS) || os(macOS) || os(tvOS) || os(watchOS)
import CoreLocation
public typealias Coordinate = CLLocationCoordinate2D
extension CLLocationCoordinate2D {
    func isValid() -> Bool {
        return CLLocationCoordinate2DIsValid(self)
    }
}
#else
public struct Coordinate {
    public var latitude: Double
    public var longitude: Double
    func isValid() -> Bool {
        return abs(latitude) <= 90 && abs(longitude) <= 180
    }
}
#endif

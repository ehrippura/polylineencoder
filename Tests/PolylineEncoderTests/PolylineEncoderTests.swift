/**
 Copyright Wayne Lin <ehrippura@gmail.com>

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

import XCTest
@testable import PolylineEncoder

final class PolylineEncoderTests: XCTestCase {

    // (38.5, -120.2), (40.7, -120.95), (43.252, -126.453)
    // _p~iF~ps|U_ulLnnqC_mqNvxq`@
    func testEncoding() {
        let result = PolylineEncoder.encode(points: [
            .init(latitude: 38.5, longitude: -120.2),
            .init(latitude: 40.7, longitude: -120.95),
            .init(latitude: 43.252, longitude: -126.453),
        ])
        XCTAssertEqual("_p~iF~ps|U_ulLnnqC_mqNvxq`@", result)
    }

    func testDecode() {
        let target = "`~oia@`~oia@"
        let coords = PolylineEncoder.decode(encoded: target)

        XCTAssertEqual(coords.first!.latitude, -179.98321)
        XCTAssertEqual(coords.first!.longitude, -179.98321)
    }

    static var allTests = [
        ("testEncoding", testEncoding),
        ("testDecode", testDecode),
    ]
}
